<?php

/**
 * @file
 * Provides an action that clears a feeds_item hash when a product is provided.
 */

/**
 * Implements hook_rules_action_info().
 *
 * Declares any meta-data about actions for Rules.
 */
function commerce_product_clear_feeds_hash_rules_action_info() {
  $actions = array(
    'commerce_product_clear_feeds_hash_reset_feeds_hash' => array(
      'label' => t('Resets the feeds hash on a product'),
      'group' => t('Feeds'),
      'parameter' => array(
        'commerce_product' => array(
          'type' => 'commerce_product',
          'label' => t('Line item'),
        ),
      ),
    ),

  );

  return $actions;
}

/**
 * Where a product is provided, then the hash in the
 * feeds_item table needs to be reset in order for feeds to be
 * aware the stock level has been updated.
 *
 * @param $product
 *   A line item object.
 */
function commerce_product_clear_feeds_hash_reset_feeds_hash($product) {

  // Feeds uses entity_id and Drupal Commerce uses product_id
  // They happen to be the same thing.
  $product_id = $product->product_id;

    db_update('feeds_item')
      ->fields(array('hash' => ''))
      ->condition('entity_type', 'commerce_product')
      ->condition('entity_id', $product_id, '=')
      ->execute();
}
