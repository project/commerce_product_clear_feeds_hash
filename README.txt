CONTENTS OF THIS FILE
---------------------

 * Description and Benefits
 * Installation and Usage
 * More Information


DESCRIPTION AND BENEFITS
------------------------

This module provides an action which will clear the feeds_item hash for the
commerce product given.

This can be useful when you need fine grain control over updating products
from a stock file, even when the source has not changed. Normally feeds 
generates a hash of the data line for the entity to be updated or imported.
When the next import is run feeds compares this hash with the new information
in the data source, the update will occur if the data is different. Feeds does
have a switch to disable the hash check, however this will affect all entities
in the file.

One possible use case for this module is ensuring that a product which has been
changed on the website (Perhaps by commerce stock) will always be updated when
a new stock file is uploaded to the website. This ensures that the product will
always reflect the stock levels of the stock control system, even if the hashed
line is matches the new stock file. This becomes relevent if a customer has
purchased an item (decrementing stock) but replacement stock set the total
stock to the same amount as the last stock file. On the next update the hash
will match and feeds will not update the product.


INSTALLATION AND USAGE
----------------------

The Commerce Product Clear Feeds Hash module is installed in the usual way. See
http://drupal.org/documentation/install/modules-themes/modules-7.
To start using the module, go to admin/config/workflow/rules, create a rule
which passes a Commerce Product to the action.

Note: If you are attempting to get a product from a line item, use the
"Entity has field" condition as suggested by rszrama in this issue
(https://www.drupal.org/node/1381964).

 

MORE INFORMATION
----------------

- This module is sponsored by: http://didymodesigns.com.au
  Didymo Designs is available for Drupal work.
- This module is released under the GPL.
- Feedback welcome, enjoy!
- ashley.maher@didymodesigns.com.au 
  https://www.drupal.org/u/didymo
